#!/bin/bash

cd exporters
count=0
ansible all --list-hosts | tail -n +2 | while read line; do
  ((count=$count+1))
  echo "${line}"
  sed -i "s/BACK_${count}_DNS/${line}/g" ../prometheus/configs/prometheus.yml
done