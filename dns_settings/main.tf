terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "3.7.0"
    }
  }
  backend "http" {}

}

variable "monitoring_ip" {
  type = string
}

variable "grafana_ip" {
  type = string
}

variable "email" {
  type = string
}


variable "cloudflare_api_token" {
  type = string
}


variable "cloudflare_zone_id" {
  type = string
}


provider "cloudflare" {
  email     = var.email
  api_token = var.cloudflare_api_token
}

resource "cloudflare_record" "grafana_record" {
  name    = "grafana"
  value   = var.grafana_ip
  type    = "A"
  proxied = true
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "monitoring_record" {
  name    = "monitoring"
  value   = var.monitoring_ip
  type    = "A"
  proxied = true
  zone_id = var.cloudflare_zone_id
}
