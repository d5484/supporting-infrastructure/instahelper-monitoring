# instahelper-monitoring

__Проект мониторинга сервиса instahelper__

__ВАЖНО__ - дефолтные порты изменены, используются следующим порты:
1. PROMETHEUS -> 80
2. ALERTMANAGER -> 2052
3. GRAFANA -> 80

#  Пререквизиты
1. Установленный __ansible__
2. Переменные окружения aws для доступа к ec2 инстансам

## Prometheus
Описание установки и настрйоки находится в __README.md__ в папке __/prometheus__

## Exporters
Для того, чтобы развернуть экспортеры на ноды в облако aws выполните
следующую команду `cd exporters и далее запустите плейбук передав параметр hosts
с названием группы ваших хостов зависящего от окружения(по-умолчанию test или production)
```bash
ansible-playbook install_exporters_playbook.yml -e "hosts=tag_instance_(test|production)"
```
()

## Grafana
Описание установки и настрйоки находится в __README.md__ в папке __/grafana__

### DNS записи настраиваются с помощью `terraform` в папке ___/dns_settings___