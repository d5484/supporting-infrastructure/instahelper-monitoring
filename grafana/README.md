# Grafana

## Install
1. Заменить ip или dns в файле grafana_public_ip
2. Выполнить следующий код и дождаться его выполнения:
```bash
cd grafana
ansible-playbook install-grafana-playbook.yml
```

## Settings
1. Войти в приложение по 3000 порту
2. Настроить в качестве дата провайдера прометеус (указать его ip)
3. Заимпортить в дашборды соодержимое файла dashboard_for_import.json 