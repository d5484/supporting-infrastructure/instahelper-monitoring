#!/bin/bash

/usr/bin/docker run \
--detach \
--name=prometheus \
--volume=/usr/local/configs/prometheus.yml:/etc/prometheus/prometheus.yml \
--volume=/usr/local/configs/alertrules.yml:/etc/prometheus/alertrules.yml \
--add-host=host.docker.internal:host-gateway \
--publish=80:9090 \
prom/prometheus:latest \
--config.file=/etc/prometheus/prometheus.yml