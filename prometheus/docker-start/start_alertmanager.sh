#!/bin/bash

/usr/bin/docker run \
    --detach \
    --name=alertmanager \
    -p 2052:9093 \
    -v /usr/local/configs/alertrules.yml:/etc/prometheus/alertrules.yml \
    -v /usr/local/configs/alertrules.yml:/etc/alertmanager/alertrules.yml \
    --add-host=host.docker.internal:host-gateway \
    prom/alertmanager:latest