# Prometheus

### Структура проекта:
1. ___/ansible_vscale_io___ папка с ansible библиотекой для развертывания инфраструктуры на vds.selectel(vscale.io)
2. ___/configs___ папка с конфигурациями prometheus и alert rules
3. ___/docker-start___ папка со скриптами запуска контейнеров
4. ___/playbooks___ ansible плэйбуки
5. ___/roles___ ansible роли
6. ___/systemd___ service файлы для системы инициализации systemd

## Установка
```bash
./extract_hosts.sh
cd prometheus
ansible-playbooks ./playbooks/install_monitoring_playbook.yaml
```